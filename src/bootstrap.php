<?php

return [
    'autoload' => [
        \CohesionUserFlow\App\Userflow::class
    ],

    'settings' => [
        CohesionUserFlow\App\Settings::class
    ],

    'yootheme' => [
        
    ]
];