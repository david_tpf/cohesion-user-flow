<?php
/*
 * ----------------------------------------------------------------------------------
 * Cohesion Userflow.
 * ----------------------------------------------------------------------------------
 * Cohesion Plugin that provides custom Userflow functionality made by The Pattern
 * Factory.
 *
 * @wordpress-plugin
 * Plugin Name:       Cohesion Userflow
 * Plugin URI:        https://thepatternfactory.co.uk
 * Description:       Custom Userflow functionaility Created by The Pattern Factory
 * Version:           1.0.0
 * Author:            The Pattern Factory
 * Author URI:        https://thepatternfactory.co.uk
 * License:           GPL version 3 or any later version
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain:       cohesion
 * Domain Path:       /languages
 * ----------------------------------------------------------------------------------
 */
use Cohesion\App\Application;

add_action('plugins_loaded', function () {
    if (!class_exists(Application::class, false)) {
        return;
    }

    $application = Application::getInstance();
    $application->load(__DIR__.'/src/bootstrap.php');
});